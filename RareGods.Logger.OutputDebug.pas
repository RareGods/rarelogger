﻿unit RareGods.Logger.OutputDebug;

interface

uses
  RareGods.Logger;

Type
  TrgLogOutputDebug = Class(TrgWriter)
  protected
    Procedure DebugMsg(Const Message: String);
  public
    Procedure Write(Const Item: TrgLogItem); override;
    Constructor Create; override;
  End;

implementation

uses
  Winapi.Windows;

{ TrgLogDebugOutput }
constructor TrgLogOutputDebug.Create;
begin
  inherited;
  FPlatformSupport := [TSuppPlatform.pfWindows];
  Self.Enabled := IsSupportedOS;
end;

procedure TrgLogOutputDebug.DebugMsg(const Message: String);
begin
  OutputDebugString(PWideChar(Message));
end;

procedure TrgLogOutputDebug.Write(const Item: TrgLogItem);
begin
  DebugMsg(LogItemToFormatedString(FormatOutput, Item));
end;

Initialization

rgLogger.LogWriters.Add(TrgLogOutputDebug.Create);

end.
