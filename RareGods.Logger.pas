﻿unit RareGods.Logger;

interface

uses
{$IFDEF DEBUG_ENGINE}
  DebugEngine.Trace,
  DebugEngine.DebugInfo,
  DebugEngine.Core,
  DebugEngine.HookException, // Bonus
{$ENDIF}
  System.SysUtils,
  System.Generics.Collections,
  System.SyncObjs;

Type
{$SCOPEDENUMS ON}
  TrgLogLevel = (Trace, Debug, Info, Warning, Error, Critical);
{$SCOPEDENUMS OFF}

  TrgLogItem = Class
  private
    FLogLevel: TrgLogLevel;
    FMessage: String;
    FTimeStamp: TDateTime;
    Function GetLogLevelString: String;
  Protected
  public
    Constructor Create; overload;
    Constructor Create(Const AMessage: String; ALogLevel: TrgLogLevel =
{$IFDEF DEBUG}
      TrgLogLevel.Debug
{$ELSE}
      TrgLogLevel.Info
{$ENDIF}
      ); overload;
  published
    property LogLevel: TrgLogLevel read FLogLevel write FLogLevel;
    property LogLevelString: String read GetLogLevelString;
    property Message: String read FMessage write FMessage;
    property TimeStamp: TDateTime read FTimeStamp write FTimeStamp;
  End;

  TrgWriter = Class
  Protected Type
{$SCOPEDENUMS ON}
    TSuppPlatform = TOSVersion.TPlatform;
{$SCOPEDENUMS OFF}
    TSuppPlatforms = set of TSuppPlatform;
  Public Const
    FO_TIME = '#TIME#';
    FO_MESSAGE = '#MESSAGE#';
    FO_LOG_LEVEL = '#LOG_LEVEL#';
  private
    FFormatOutput: String;
    FFormatDateTime: String;
    FEnabled: Boolean;
  protected
    FCS: TCriticalSection;
    FPlatformSupport: TSuppPlatforms;
    Function LogItemToFormatedString(Const FrmtStr: String; LogItem: TrgLogItem): String;
    Function IsSupportedOS: Boolean;
  Public
    Procedure Write(Const rgLogItem: TrgLogItem); Virtual; abstract;
    Constructor Create; Virtual;
    destructor Destroy; override;
  published
    property Enabled: Boolean read FEnabled write FEnabled;
    property PlatformSupport: TSuppPlatforms read FPlatformSupport;
    property FormatOutput: String read FFormatOutput write FFormatOutput;
    property FormatTime: String read FFormatDateTime write FFormatDateTime;
  End;

{$IFDEF DEBUG_ENGINE}

  TrgStackItem = Class
    Adress: String;
    Module: String;
    UnitName: String;
    Method: String;
    Constructor Create(PItem: PStackItem);
  End;
{$ENDIF}

  TrgOnLog = reference to Procedure(Item: TrgLogItem);

  TrgLogger = Class
  private
    FLogWriters: TObjectList<TrgWriter>;
    FLogWritersLevel: TrgLogLevel;
    FOnLog: TrgOnLog;
{$IFDEF DEBUG_ENGINE}
    FStackCall: TObjectList<TrgStackItem>;
    function GetStackCall: TObjectList<TrgStackItem>;
{$ENDIF}
  public
    Procedure Write(Const Item: TrgLogItem); overload;
    Procedure Write(Const Msg: String; LogLevel: TrgLogLevel); overload;
    Procedure MethodEnter(Const MethodName: String);
    Procedure MethodLeave(Const MethodName: String);
    constructor Create;
    destructor Destroy; override;
  published
    property LogWriters: TObjectList<TrgWriter> read FLogWriters Write FLogWriters;
    property LogWritersLevel: TrgLogLevel read FLogWritersLevel
      Write FLogWritersLevel default
{$IFDEF DEBUG}
      TrgLogLevel.Debug
{$ELSE}
      TrgLogLevel.Info
{$ENDIF};
    property OnLog: TrgOnLog read FOnLog write FOnLog;
{$IFDEF DEBUG_ENGINE}
    property Stack: TObjectList<TrgStackItem> read GetStackCall;
{$ENDIF}
  End;

Var
  rgLogger: TrgLogger;

implementation

uses

  System.TypInfo;

{ TrgLogger }

constructor TrgLogger.Create;
begin
  FLogWriters := TObjectList<TrgWriter>.Create;
{$IFDEF DEBUG}
  LogWritersLevel := TrgLogLevel.Debug;
{$ELSE}
  LogWritersLevel := TrgLogLevel.Info;
{$ENDIF}
{$IFDEF DEBUG_ENGINE}
  FStackCall := TObjectList<TrgStackItem>.Create;
{$ENDIF}
end;

destructor TrgLogger.Destroy;
begin
{$IFDEF DEBUG_ENGINE}
  FStackCall.Free;
{$ENDIF}
  FLogWriters.Free;
  inherited;
end;
{$IFDEF DEBUG_ENGINE}

function TrgLogger.GetStackCall: TObjectList<TrgStackItem>;
var
  lStackInfo: TStackInfo;
  lStack: TCallTrace;
  I: Integer;
begin
  GetStackInfo(lStackInfo);
  lStack := TCallTrace.Create;
  lStack.StackInfo := lStackInfo;
  FStackCall.Clear;
  try
    lStack.Trace;
    for I := 0 to Stack.Count - 1 do
      FStackCall.Add(TrgStackItem.Create(lStack.Items[I]));
  finally
    lStack.Free;
  end;
  Result := FStackCall;
end;
{$ENDIF}

procedure TrgLogger.MethodEnter(const MethodName: String);
begin
  Self.Write('Enter in ' + MethodName, TrgLogLevel.Trace);
end;

procedure TrgLogger.MethodLeave(const MethodName: String);
begin
  Self.Write('Leave from ' + MethodName, TrgLogLevel.Trace);
end;

procedure TrgLogger.Write(const Msg: String; LogLevel: TrgLogLevel);
var
  LogItem: TrgLogItem;
begin
  LogItem := TrgLogItem.Create(Msg, LogLevel);
  try
    Write(LogItem);
  finally
    LogItem.Free;
  end;
end;

procedure TrgLogger.Write(Const Item: TrgLogItem);
var
  I: Integer;
begin
  if Item.LogLevel < LogWritersLevel then
    Exit;
  for I := 0 to FLogWriters.Count - 1 do
  begin
    if FLogWriters[I].Enabled then
      FLogWriters[I].Write(Item);
  end;
  if Assigned(OnLog) then
    OnLog(Item);
end;
{ TrgLogItem }

constructor TrgLogItem.Create(Const AMessage: String; ALogLevel: TrgLogLevel);
begin
  Create;
  FMessage := AMessage;
  FLogLevel := ALogLevel;
end;

{ TrgWriter }

constructor TrgWriter.Create;
begin
  FPlatformSupport := [];
  Enabled := False;
  FFormatDateTime := 'dd/mm/yyyy hh:mm:ss';
  FormatOutput := '[' + FO_TIME + '] ' + '(' + FO_LOG_LEVEL + '): ' + FO_MESSAGE;
  FCS := TCriticalSection.Create;
end;

destructor TrgWriter.Destroy;
begin
  FCS.Free;
  inherited;
end;

function TrgWriter.IsSupportedOS: Boolean;
begin
  Result := TOSVersion.Platform in PlatformSupport;
end;

function TrgWriter.LogItemToFormatedString(const FrmtStr: String;
  LogItem: TrgLogItem): String;
begin
  Result := FrmtStr;
  Result := Result.Replace(FO_TIME, FormatDateTime(FFormatDateTime, LogItem.FTimeStamp));
  Result := Result.Replace(FO_MESSAGE, LogItem.Message);
  Result := Result.Replace(FO_LOG_LEVEL, LogItem.LogLevelString)
end;

constructor TrgLogItem.Create;
begin
  FMessage := '';
  FTimeStamp := Now;
  FLogLevel := TrgLogLevel.{$IFDEF DEBUG}Debug{$ELSE}Info{$ENDIF};
end;

function TrgLogItem.GetLogLevelString: String;
begin
  Result := GetEnumName(TypeInfo(TrgLogLevel), Ord(FLogLevel));
end;
{$IFDEF DEBUG_ENGINE}
{ TrgStackItem }

constructor TrgStackItem.Create(PItem: PStackItem);
var
  LInfo: TAddressInfo;
begin
  if GetAddressInfo(PItem^.Info.Address, LInfo) then
  begin
    Self.Adress := Format('%p', [PItem^.CallAddress]);
    Self.Module := LInfo.DebugSource.Module.BaseName;
    Self.UnitName := LInfo.UnitName;
  end;
end;
{$ENDIF}

initialization

rgLogger := TrgLogger.Create;

Finalization

FreeAndNil(rgLogger);

end.
