﻿unit RareGods.Logger.OutputFile;

interface

uses
  RareGods.Logger;

Type
  TrgLogOutputFile = Class(TrgWriter)
  private
    FFileName: String;
  public
    Procedure Write(Const Item: TrgLogItem); override;
    constructor Create(Const AFileName: String); overload;
    constructor Create; overload; override;
  published
    property FileName: String read FFileName write FFileName;
  End;

implementation

uses
  System.SysUtils,
  System.IOUtils;
{ TrgLogOutputFile }

constructor TrgLogOutputFile.Create;
begin
  inherited;
  FPlatformSupport := [TSuppPlatform.pfWindows, TSuppPlatform.pfAndroid,
    TSuppPlatform.pfMacOS, TSuppPlatform.pfiOS];
  Self.Enabled := IsSupportedOS;
  Self.FileName := ExtractFilePath(ParamStr(0)) + FormatDateTime('yyyy-mm-dd',
    Now) + '.log';
end;

constructor TrgLogOutputFile.Create(const AFileName: String);
begin
  Self.Create;
  Self.FileName := AFileName;
end;

procedure TrgLogOutputFile.Write(const Item: TrgLogItem);
begin
  inherited;
  FCS.Acquire;
  try
    TFile.AppendAllText(FFileName, LogItemToFormatedString(FormatOutput, Item) + #13#10,
      TEncoding.UTF8);
  finally
    FCS.Release;
  end;
end;

end.
