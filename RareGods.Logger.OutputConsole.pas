﻿unit RareGods.Logger.OutputConsole;

interface

uses
  RareGods.Logger;

Type
  TrgConsoleColor = (Black = 0, Blue = 1, Green = 2, Cyan = 3, Red = 4, Magenta = 5,
    Brown = 6, LightGray = 7, DarkGray = 8, LightBlue = 9, LightGreen = 10,
    LightCyan = 11, LightRed = 12, LightMagenta = 13, Yellow = 14, White = 15);

  TrgLogOutputConsole = Class(TrgWriter)
  private
    FHandle: Cardinal;
  public
    procedure SetColor(Const Text, background: Integer); overload;
    procedure SetColor(Const Text, background: TrgConsoleColor); overload;
    Procedure Write(Const Item: TrgLogItem); override;
    Constructor Create; override;
    destructor Destroy; override;
  End;

implementation

uses
  Winapi.Windows;

{ TrgLogOutputConsole }

constructor TrgLogOutputConsole.Create;
begin
  inherited;
  AllocConsole;
  FHandle := GetStdHandle(STD_OUTPUT_HANDLE);
  FPlatformSupport := [TSuppPlatform.pfWindows];
  Self.Enabled := IsSupportedOS;
end;

destructor TrgLogOutputConsole.Destroy;
begin
  FreeConsole;
  inherited;
end;

procedure TrgLogOutputConsole.SetColor(const Text, background: TrgConsoleColor);
begin
  SetColor(Integer(Text), Integer(background));
end;

procedure TrgLogOutputConsole.SetColor(const Text, background: Integer);
begin
  SetConsoleTextAttribute(FHandle, ((background shl 4) or Text));
end;

procedure TrgLogOutputConsole.Write(const Item: TrgLogItem);
begin
  inherited;
  case Item.LogLevel of
    TrgLogLevel.Debug:
      SetColor(TrgConsoleColor.White, TrgConsoleColor.Black);
    TrgLogLevel.Info:
      SetColor(TrgConsoleColor.LightGreen, TrgConsoleColor.Black);
    TrgLogLevel.Warning:
      SetColor(TrgConsoleColor.Yellow, TrgConsoleColor.Black);
    TrgLogLevel.Error:
      SetColor(TrgConsoleColor.LightRed, TrgConsoleColor.Black);
    TrgLogLevel.Critical:
      SetColor(TrgConsoleColor.Black, TrgConsoleColor.LightRed);
  end;
  WriteLn(LogItemToFormatedString(FormatOutput, Item));
end;

end.
