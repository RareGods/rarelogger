program Test;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  Winapi.Windows,
  System.IOUtils,
  System.SysUtils,
  RareGods.Logger in 'RareGods.Logger.pas',
  RareGods.Logger.OutputDebug in 'RareGods.Logger.OutputDebug.pas',
  RareGods.Logger.OutputConsole in 'RareGods.Logger.OutputConsole.pas',
  RareGods.Logger.OutputFile in 'RareGods.Logger.OutputFile.pas',
  RareGods.Logger.OutputSocket in 'RareGods.Logger.OutputSocket.pas';

Procedure Go;
Begin
  rgLogger.LogWriters.Add(TrgLogOutputConsole.Create);
  rgLogger.Write(TrgLogItem.Create('Debug', TrgLogLevel.Debug));
  rgLogger.Write(TrgLogItem.Create('Info', TrgLogLevel.Info));
  rgLogger.Write(TrgLogItem.Create('Warning', TrgLogLevel.Warning));
  rgLogger.Write(TrgLogItem.Create('Error', TrgLogLevel.Error));
  rgLogger.Write(TrgLogItem.Create('Critical', TrgLogLevel.Critical));
  raise Exception.Create('Error Message');
End;

begin
  try
    Go;
  except
    on E: Exception do
      Writeln(E.ClassName + ': ' + E.Message);
  end;
  Readln;
end.
