﻿unit RareGods.Logger.OutputSocket;

interface

uses
  System.Net.Socket,
  RareGods.Logger;

Type
  TrgLogOutputSocket = Class(TrgWriter)
  private
    FServer: String;
    FSock: TSocket;
    FPort: Word;
  public
    Procedure Write(Const Item: TrgLogItem); override;
    constructor Create; overload; override;
    destructor Destroy; override;
  published
    property Server: String read FServer write FServer;
    property Port: Word read FPort write FPort;
  End;

implementation

{ TrgLogOutputSocket }

constructor TrgLogOutputSocket.Create;
begin
  inherited;
  FSock := TSocket.Create;
  FSock.Connect(FServer, '', '', FPort);
end;

destructor TrgLogOutputSocket.Destroy;
begin
  FSock.Free;
  inherited;
end;

procedure TrgLogOutputSocket.Write(const Item: TrgLogItem);
begin
  inherited;
  FSock.Send(LogItemToFormatedString(FormatOutput, Item) + #13#10);
end;

end.
