unit RareGods.Logger.OutputRichViewVCL;

interface

uses
  Vcl.ComCtrls,
  RareGods.Logger,
  System.UITypes;

Type
  TrgLogOutputRichEditVCL = Class(TrgWriter)
  private
    FRichEdit: TRichEdit;
    FColorDebug: TColor;
    FColorInfo: TColor;
    FColorWarning: TColor;
    FColorCritical: TColor;
    FColorError: TColor;
  public
    Procedure Write(Const Item: TrgLogItem); override;
    constructor Create; overload; override;
    constructor Create(RichEdit: TRichEdit); overload;
  published
    property RichEdit: TRichEdit read FRichEdit write FRichEdit;
    property ColorDebug: TColor read FColorDebug write FColorDebug;
    property ColorInfo: TColor read FColorInfo write FColorInfo;
    property ColorWarning: TColor read FColorWarning write FColorWarning;
    property ColorError: TColor read FColorError write FColorError;
    property ColorCritical: TColor read FColorCritical write FColorCritical;
  End;

implementation

{ TrgLogOutputConsole }

constructor TrgLogOutputRichEditVCL.Create;
begin
  inherited;
  ColorDebug := TColorRec.Black;
  ColorInfo := TColorRec.Green;
  ColorWarning := TColorRec.Yellow;
  ColorError := TColorRec.Red;
  ColorCritical := TColorRec.Magenta;
  Self.FPlatformSupport := [TSuppPlatform.pfWindows];
  Self.Enabled:=IsSupportedOS;
end;

constructor TrgLogOutputRichEditVCL.Create(RichEdit: TRichEdit);
begin
  Create;
  FRichEdit := RichEdit;
end;

procedure TrgLogOutputRichEditVCL.Write(const Item: TrgLogItem);
begin
  inherited;
  if not Assigned(FRichEdit) then
    Exit;
  case Item.LogLevel of
    TrgLogLevel.Debug:
      FRichEdit.SelAttributes.Color := ColorDebug;
    TrgLogLevel.Info:
      FRichEdit.SelAttributes.Color := ColorInfo;
    TrgLogLevel.Warning:
      FRichEdit.SelAttributes.Color := ColorWarning;
    TrgLogLevel.Error:
      FRichEdit.SelAttributes.Color := ColorError;
    TrgLogLevel.Critical:
      FRichEdit.SelAttributes.Color := ColorCritical;
  end;
  FRichEdit.SelText := LogItemToFormatedString(FormatOutput, Item) + #13#10;
end;

end.
